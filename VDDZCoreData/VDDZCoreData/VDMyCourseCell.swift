//
//  VDMyCourseCellTableViewCell.swift
//  VDDZCoreData
//
//  Created by Victor Deryabin on 19.07.20.
//  Copyright © 2020 Victor Deryabin. All rights reserved.
//

import UIKit

class VDMyCourseCell: UITableViewCell, ConfiguringCell {
    @IBOutlet var name: UILabel?
    @IBOutlet var predmet: UILabel?
    @IBOutlet var prepod: UILabel?

    func configureCell(withObject object: Special) {
        let course = object as! VDCourseSpecial
        name?.text = course.name
        predmet?.text = course.predmet
        prepod?.text = String("\(course.prepod?.firstName ?? "Default") \(course.prepod?.lastName ?? "Prepod")")
    }
}
