//
//  Special.swift
//  VDDZCoreData
//
//  Created by Victor Deryabin on 22.07.20.
//  Copyright © 2020 Victor Deryabin. All rights reserved.
//

import UIKit
import CoreData
protocol CompareTwoElements {
    func compareTwoElements(i2: CompareTwoElements, ByParameter parameter: String) -> Bool?
    subscript(string:String) -> String? { get }
}

class Special: NSObject, CompareTwoElements {
    var ID:NSManagedObjectID?
    func compareTwoElements(i2: CompareTwoElements, ByParameter parameter: String) -> Bool? {
        if let i22 = i2[parameter], let i11 = self[parameter] {
            if (i22 > i11) {
                return true
            }
            if (i22 == i11) {
                return nil
            }
        }
        return false
    }
    subscript (string:String) -> String? {
        return nil
    }
}
