//
//  VDDetailCell.swift
//  VDDZCoreData
//
//  Created by Victor Deryabin on 13.07.20.
//  Copyright © 2020 Victor Deryabin. All rights reserved.
//

import UIKit

protocol TextFieldChanged {
    func changeDictionaryData(name: String, value: String)
}

protocol CallingPopoverByPicker {
    func callPopover(cell: VDDetailCell)
}

class VDDetailCell: UITableViewCell,UITextFieldDelegate {
    @IBOutlet var label: UILabel?
    var cellMeaning: String?
    var delegate1: TextFieldChanged?
    var delegate2: CallingPopoverByPicker?
    @IBOutlet var txtField: UITextField?
   
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate1?.changeDictionaryData(name: cellMeaning!, value: textField.text!)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if delegate2 != nil  {
        
            delegate2?.callPopover(cell: self)
            return false
        }
        return true
    }
}
