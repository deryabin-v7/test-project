//
//  VDCourseDetailTableViewController.swift
//  VDDZCoreData
//
//  Created by Victor Deryabin on 14.07.20.
//  Copyright © 2020 Victor Deryabin. All rights reserved.
//

import UIKit
import CoreData

protocol DidCourseDataChange {
    func changeCourseDataAt(object: VDCourseSpecial)
}
class VDCourseDetailTableViewController: UITableViewController,TextFieldChanged,DidUserDataChange,UIPopoverPresentationControllerDelegate,CallingPopoverByPicker,ChangeStudentsOfCourse,ChangePrepodOfCourse {
    
    var course: VDCourseSpecial?
    var pickerCellMeaning: String?
    var delegate: DidCourseDataChange?
    var nameTFData = ["name","predmet"]
    var newData = [String:String]()
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 || indexPath.section == 0 && indexPath.row == nameTFData.count {
        return 90
        }
        return 50
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 300
        self.tableView.rowHeight = UITableView.automaticDimension
        var nib = UINib.init(nibName: "detailcell", bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: "UserDetailCell")
        nib = UINib.init(nibName: "userDescriptionCell", bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: "UserCell")
        let barb = UIBarButtonItem.init(title: "Save", style: .plain, target: self, action: #selector(saveData(_:)))
        self.navigationItem.setRightBarButtonItems([barb], animated: true)
        newData[nameTFData[0]] = course?.name
        newData[nameTFData[1]] = course?.predmet
    }
    
    override func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath)
    {
        if indexPath.section == 1 && indexPath.row == 0 {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.backgroundColor = UIColor.lightGray
        }
    }
    
    override func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        if indexPath.section == 1 && indexPath.row == 0 {
            cell?.backgroundColor = UIColor(red: 0.0 , green: 191/255.0 , blue: 1.0 , alpha: 0.2)
        }
        else {
            cell?.backgroundColor = UIColor.white
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if indexPath.row == 0 {
            cell.backgroundColor = UIColor(red: 0.0 , green: 191/255.0 , blue: 1.0 , alpha: 0.2)
            }
            cell.selectionStyle = .none
        }
        if (indexPath.section == 0) && (0...1).contains(indexPath.row) {
            cell.selectionStyle = .none
        }
    }
    
    func changeStudsOfCourse( checkedStudents: [Bool]) {
        for index in 0 ..< checkedStudents.count {
            if checkedStudents[index] == false {
                VDDataManager.sharedManager.resignUserAsStudent(with: VDUserSpecial.users[index].ID!, fromCourseWith: (course?.ID)!)
            }
            else {
                VDDataManager.sharedManager.assignUserAsStudent(with: VDUserSpecial.users[index].ID!, onCourseWith: (course?.ID)!)
            }
        }
    }
    
    func changePrepodOfCourse(checkedStudent: Int?) {
        if course?.prepod != nil {
            if checkedStudent == nil {
                 VDDataManager.sharedManager.resignUserAsTeacher(with: (course?.prepod?.ID!)!, fromCourseWith: (course?.ID)!)
            }
            else {
                let studID = VDUserSpecial.users[checkedStudent!].ID
                if studID != course?.prepod?.ID {
                    VDDataManager.sharedManager.resignUserAsTeacher(with: (course?.prepod?.ID!)!, fromCourseWith: (course?.ID)!)
                    VDDataManager.sharedManager.assignUserAsTeacher(with: studID!, onCourseWith: (course?.ID)!)
                }
            }
        }
        else {
            if checkedStudent != nil {
                let studID = VDUserSpecial.users[checkedStudent!].ID
                VDDataManager.sharedManager.assignUserAsTeacher(with: studID!, onCourseWith: (course?.ID)!)
            }
        }
    }
    
    func callPopover(cell: VDDetailCell) {
        let pv = storyboard?.instantiateViewController(withIdentifier: "PickerController") as! VDPickerController
        pv.delegate1 = self
        pv.cellMeaning = cell.cellMeaning
        let textField = cell.txtField
        if cell.txtField?.text != nil {
            pv.initialTitle = cell.txtField?.text
        }
        pv.modalPresentationStyle = UIModalPresentationStyle.popover
        pv.preferredContentSize = CGSize(width: 300, height: 300)
        pv.picker?.backgroundColor = UIColor.white
            self.present(pv, animated: true, completion: nil)
        let popover = pv.popoverPresentationController
        popover?.permittedArrowDirections = .any
        popover?.delegate = self
        popover?.sourceView = textField
        popover?.sourceRect = (textField?.bounds)!
    }
    
    func changeUserDataAt(object: VDUserSpecial) {
        if course?.prepod == nil {
        let id = VDDataManager.sharedManager.addEmptyUser()
        VDDataManager.sharedManager.assignUserAsTeacher(with: id, onCourseWith: (course?.ID)!)
        object.ID = id
        }
        VDDataManager.sharedManager.updateUser(user: object)
        VDDataManager.sharedManager.updateCourseBD()
        tableView?.reloadData()
    }
    
    func callDetailViewController( myIndexPath: NSIndexPath) {
        let vc =  self.storyboard?.instantiateViewController(withIdentifier: "detailUserID") as! VDUserDetailControllerTableViewController
        vc.delegate = self as DidUserDataChange
        if myIndexPath.section == 1 {
            vc.user = (course?.students[myIndexPath.row - 1])!
        }
        else if myIndexPath.section == 0 {
            vc.user = (course?.prepod)
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        VDDataManager.sharedManager.updateUserBD()
        VDDataManager.sharedManager.updateCourseBD()
        
        if course != nil {
            let ind = VDCourseSpecial.getCourseIndexByID(id: course!.ID!)
        course = VDCourseSpecial.courses[ind!]
        self.tableView?.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard.init(name: "MultipleCheckController", bundle: nil)
        if indexPath.section == 0 && indexPath.row == nameTFData.count {
            let vc = storyboard?.instantiateViewController(withIdentifier: "PrepodCheck") as! VDPrepodOfCourseCheckViewController
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            
            if course?.prepod != nil {
                vc.prepodIndex = VDUserSpecial.getUserIndexByID(id: course!.prepod!.ID!)
            }
            vc.delegate3 = self as ChangePrepodOfCourse
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.section == 1 && indexPath.row == 0{
            let vc = storyBoard.instantiateViewController(withIdentifier: "CheckController") as! VDCheckViewController
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            vc.type = .students
            var boolArray = [Bool].init(repeating: false, count: VDUserSpecial.users.count)
            for obj in (course?.students)! {
                boolArray[VDUserSpecial.getUserIndexByID(id: obj.ID!)!] = true
            }
            vc.checked = boolArray
            vc.delegate5 = self as ChangeStudentsOfCourse
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
  
    @objc func saveData(_ but: UIBarButtonItem) {
        tableView.endEditing(true)
        course?.name =  newData[nameTFData[0]]
        course?.predmet = newData[nameTFData[1]]
        delegate?.changeCourseDataAt(object: course!)
        self.navigationController?.popViewController(animated: true)
    }
    
    func changeDictionaryData(name: String, value: String) {
        newData[name] = value
        if name == nameTFData[1] {
            let cell = tableView.cellForRow(at: IndexPath.init(row: 1, section: 0)) as! VDDetailCell
            cell.txtField?.text = value
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Course information"
        case 1:
            return "Students"
        default:
            NSLog("ERROR IN titleForHeaderInSection IN COURSE DETAIL VC!!!\n\n")
            return ""
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return nameTFData.count + 1
        case 1:
            return (course?.students.count)! + 1
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        let identifier: String

        switch indexPath.section {
        case 0: if indexPath.row == 2 {
            var cell = VDMyCell.init()
            identifier = "UserCell"
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! VDMyCell
            if course?.prepod != nil {
                cell.firstName?.text = course?.prepod?.firstName
                cell.lastName?.text = course?.prepod?.lastName
            }
            else {
                cell.firstName?.text = "There's no"
                cell.lastName?.text = " teacher"
            }
            cell.adress?.text = "Prepod"
            return cell
        }
        else {
            var cell = VDDetailCell.init()
            identifier = "UserDetailCell"
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! VDDetailCell
            cell.cellMeaning = nameTFData[indexPath.row]
            cell.delegate1 = self as TextFieldChanged
            if indexPath.row == 1 {
                cell.delegate2 = self
            }
            cell.label?.text = nameTFData[indexPath.row]
            cell.txtField?.text = newData[nameTFData[indexPath.row]]
            return cell
            }
        case 1:
            let row = indexPath.row - 1
            var cell = VDMyCell()
            identifier = "UserCell"
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! VDMyCell
            if indexPath.row == 0 {
                cell.firstName?.text = " "
                cell.lastName?.text = " "
                cell.adress?.text = "Add Student"
                
                return cell
            }
            cell.firstName?.text = course?.students[row].firstName
            cell.lastName?.text = course?.students[row].lastName
            cell.adress?.text = course?.students[row].adress
            return cell
        default: break
        }
        return cell
    }
}

