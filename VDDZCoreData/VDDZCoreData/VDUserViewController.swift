//
//  VDUserViewController.swift
//  VDDZCoreData
//
//  Created by Victor Deryabin on 12.07.20.
//  Copyright © 2020 Victor Deryabin. All rights reserved.
//

import UIKit
import CoreData

class VDUserViewController: ViewController, DidUserDataChange{
    var temporaryUserID: NSManagedObjectID?
        override func viewDidLoad() {
        super.viewDidLoad()
        
        self.identifier = "UserCell"
        //VDDataManager.sharedManager.updateUserBD()
        //VDDataManager.sharedManager.updateCourseBD()
        
        dataSource.setCellType(cellIdentifier: "UserCell")
       
                let but = UIBarButtonItem.init(barButtonSystemItem: .edit, target: self, action: #selector(myEditing(but:)))
        
        
        self.navigationItem.leftBarButtonItem = but
        let but1 = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
        let but2 = UIBarButtonItem.init(title: "Show DB", style: .plain, target: self, action: #selector(showAll(_:)))
        self.navigationItem.setRightBarButtonItems([but1,but2], animated: true)
        
        // Do any additional setup after loading the view.
    }
    @objc func showAll(_ but: UIBarButtonItem) {
        VDDataManager.sharedManager.showAllObjects()
    }
    override func createDict() -> [String: [Special]]{
        return ["Users":VDUserSpecial.users]
    }
    func changeUserDataAt(object: VDUserSpecial) {
        temporaryUserID = nil
        VDDataManager.sharedManager.updateUser(user: object)
        VDDataManager.sharedManager.updateCourseBD()
        tableView?.reloadData()
        //object = fetchedResultsController.object(at: indexPath as IndexPath) as! VDUser
        
        //configureCell(_ cell: UITableViewCell, withObject object: object)
    }
    
        func callDetailViewController( myIndexPath: NSIndexPath?) {
        //var vc = VDUserDetailControllerTableViewController()
        let storyBoard = UIStoryboard.init(name: "Users", bundle: nil)
        let vc =  storyBoard.instantiateViewController(withIdentifier: "detailUserID") as! VDUserDetailControllerTableViewController
        vc.delegate = self as DidUserDataChange
        if myIndexPath == nil {
            let id = VDDataManager.sharedManager.addEmptyUser()
            vc.user = VDUserSpecial()
            vc.user?.ID = id
            temporaryUserID = id
            //VDDataManager.sharedManager.updateUser(user: vc.user!)
        }
        else {
        vc.user = dataSource.sourceDictionary[dataSource.keys[myIndexPath!.section]]![myIndexPath!.row] as? VDUserSpecial
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        callDetailViewController(myIndexPath: indexPath as NSIndexPath)
         self.tableView?.reloadData()
    }
    @objc func insertNewObject(_ sender: Any) {
        callDetailViewController(myIndexPath: nil)        
        //ЧТОБЫ БЫЛА АНИМАЦИЯ
        /*tableView?.beginUpdates()
        let iUser = VDUserSpecial.getUserIndexByID(id: id1)
        let ip = IndexPath.init(row: iUser!, section: 0)
        tableView?.insertRows(at: [ip], with: .fade)
        tableView?.endUpdates()*/
        //self.tableView?.reloadData()
        //let newObjIP = self.fetchedResultsController.indexPath(VDUserSpecial.users[users.count])
        //allDetailViewController(myIndexPath: newObjIP! as NSIndexPath)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        if temporaryUserID != nil {
            VDDataManager.sharedManager.deleteByID(id: temporaryUserID!)
            temporaryUserID = nil
        }
        VDDataManager.sharedManager.updateUserBD()
        VDDataManager.sharedManager.updateCourseBD()
        dataSource.setDataDictionary(data: createDict())
        tableView?.reloadData()
    }
    
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
