//
//  VDCourseCheckViewController.swift
//  VDDZCoreData
//
//  Created by Victor Deryabin on 20.07.20.
//  Copyright © 2020 Victor Deryabin. All rights reserved.
//

import UIKit
protocol ChangeCoursesOfStudent {
    func changeCoursesOfStud(checkedCourses: [Bool])
}

protocol ChangeCoursesForTeachingOfStudent {
    func changeCoursesForTeachingOfStud(checkedCourses: [Bool])
}

protocol ChangeStudentsOfCourse {
    func changeStudsOfCourse(checkedStudents: [Bool])
}
enum TypeOfCourse {
    case learning
    case teaching
    case students
}
class VDCheckViewController: ViewController {
    
    var checked : [Bool]?
    var delegate3: ChangeCoursesOfStudent?
    var delegate4: ChangeCoursesForTeachingOfStudent?
    var delegate5: ChangeStudentsOfCourse?
    var type: TypeOfCourse = .learning
    override func viewDidLoad() {
        super.viewDidLoad()
        switch type {
        case .teaching,.learning:
            dataSource.setCellType(cellIdentifier: "CourseCell")
        case .students:
            dataSource.setCellType(cellIdentifier: "UserCell")
        }
        self.navigationItem.rightBarButtonItems = []
        self.navigationItem.leftBarButtonItems = []
        let but = UIBarButtonItem.init(title: NSLocalizedString("Save", comment: "") , style: .plain, target: self, action: #selector(saveChoice(_:)))
        self.navigationItem.setRightBarButton(but, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        cell?.backgroundColor = UIColor.white
        
    }
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        cell?.backgroundColor = UIColor.white
        
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        for index in 0 ..< checked!.count {
            if checked?[index] == true {
                let cell = tableView?.cellForRow(at: IndexPath.init(row: index, section: 0))
                cell?.accessoryType = .checkmark
            }
        }
    }
    
    @objc func saveChoice(_ but: UIBarButtonItem) {
        switch type {
        case .learning:
            delegate3!.changeCoursesOfStud(checkedCourses: checked!)
            break
        case .teaching:
            delegate4?.changeCoursesForTeachingOfStud(checkedCourses: checked!)
            break
        case .students:
            delegate5?.changeStudsOfCourse(checkedStudents: checked!)
            break
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.selectionStyle = .none
        if cell?.accessoryType == .checkmark {
            checked?[indexPath.row] = false
            cell?.accessoryType = .none
        }
        else {
            checked?[indexPath.row] = true
            cell?.accessoryType = .checkmark
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        switch type {
        case .learning,.teaching:
            dataSource.setDataDictionary(data: ["Courses":VDCourseSpecial.courses])
        case .students:
            dataSource.setDataDictionary(data: ["Users":VDUserSpecial.users])
        }
        tableView?.reloadData()
    }
}
