//
//  VDMyCell.swift
//  VDDZCoreData
//
//  Created by Victor Deryabin on 12.07.20.
//  Copyright © 2020 Victor Deryabin. All rights reserved.
//

import UIKit
protocol ConfiguringCell {
    func configureCell(withObject object: Special)
}

class VDMyCell: UITableViewCell, ConfiguringCell {

    @IBOutlet var firstName: UILabel?
    @IBOutlet var lastName: UILabel?
    @IBOutlet var adress: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        adress?.adjustsFontSizeToFitWidth = true
    }
    
    func configureCell(withObject object: Special) {
        let user = object as! VDUserSpecial
        firstName?.text = user.firstName
        lastName?.text = user.lastName
        adress?.text = user.adress
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
