//
//  ViewController.swift
//  VDDZCoreData
//
//  Created by Victor Deryabin on 12.07.20.
//  Copyright © 2020 Victor Deryabin. All rights reserved.
//

import UIKit
import CoreData
class ViewController: UIViewController, UITableViewDelegate  {
    var info = ["1","2","3","4","5"]
    internal var identifier = "UserCell"
    var dataSource = CustomDataSourse()
    var managedObjectContext: NSManagedObjectContext? {
        get {
            return VDDataManager.sharedManager.persistentContainer.viewContext
        }
    }
    @IBOutlet var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var nib = UINib.init(nibName: "userDescriptionCell", bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: "UserCell")
        nib = UINib.init(nibName: "courseDescriptionCell", bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: "CourseCell")
        tableView?.delegate = self
        tableView?.dataSource = dataSource
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func createDict() -> [String: [Special]] {
        return [:]
    }
    
    @objc func myEditing( but: UIBarButtonItem) {
        var but :UIBarButtonItem
        if (self.tableView?.isEditing == false) {
            self.tableView?.isEditing = true
            but = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(myEditing(but:)))
        }
        else {
            self.tableView?.isEditing = false
            but = UIBarButtonItem.init(barButtonSystemItem: .edit, target: self, action: #selector(myEditing(but:)))
        }
        self.navigationItem.leftBarButtonItem = but
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
}

