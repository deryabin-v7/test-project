//
//  VDPrepodDetailViewController.swift
//  VDDZCoreData
//
//  Created by Victor Deryabin on 20.07.20.
//  Copyright © 2020 Victor Deryabin. All rights reserved.
//

import UIKit

class VDPrepodViewController: VDUserViewController {
    var nameCourse = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource.setCellType(cellIdentifier: "UserCell")
        self.navigationItem.setLeftBarButtonItems([], animated: true)
        self.navigationItem.setRightBarButtonItems([], animated: true)
            }
    override func createDict() -> [String: [Special]]{
        nameCourse = [String]()
        var dict: [String: [VDUserSpecial]] = [:]
        for case let course in VDCourseSpecial.courses {
            
            if let predmet = course.predmet {
                if nameCourse.contains(predmet) == false {
                    dict[course.predmet!] = [VDUserSpecial]()
                    nameCourse.append(course.predmet!)
                }
                
                if course.prepod != nil && ((dict[predmet]?.contains(course.prepod!)) == false) {
                    dict[predmet]!.append(course.prepod!)
                }
            }
            for i in dict.keys {
                dict[i]?.sortingBy(parameters: ["firstName","lastName"])
            }
        }
        return dict

    }
}
