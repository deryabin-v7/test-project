//
//  CustomDataSourse.swift
//  VDDZCoreData
//
//  Created by Victor Deryabin on 23.07.20.
//  Copyright © 2020 Victor Deryabin. All rights reserved.
//

import UIKit
import CoreData
enum cellType {
    case user
    case course
}

class CustomDataSourse: NSObject, UITableViewDataSource {
    var cellType: String?
    var sourceDictionary: [String: [Special]] = [:]
    var keys: [String] = []
    func setCellType(cellIdentifier:String)
    {
        cellType = cellIdentifier
        
    }
    
    func setDataDictionary( data: [String: [Special]])
    {
        sourceDictionary = data
        keys = [String]()
        for key in sourceDictionary.keys {
            keys.append(key)
        }
        keys.sort(by: <)
    }
    
    func updateData(cell:ConfiguringCell, indexPath: IndexPath) {
        cell.configureCell(withObject: sourceDictionary[keys[indexPath.section]]![indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return keys[section]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellType!) as! ConfiguringCell
        
        updateData(cell: cell, indexPath: indexPath)
        
        return cell as! UITableViewCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return keys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sourceDictionary[keys[section]]!.count
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            VDDataManager.sharedManager.deleteByID(id: sourceDictionary[keys[indexPath.section]]![indexPath.row].ID!)
            sourceDictionary[keys[indexPath.section]]!.remove(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.endUpdates()
        }
    }
}
