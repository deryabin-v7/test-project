//
//  AppDelegate.swift
//  VDDZCoreData
//
//  Created by Victor Deryabin on 12.07.20.
//  Copyright © 2020 Victor Deryabin. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        VDDataManager.sharedManager.deleteAllUsers()
        VDDataManager.sharedManager.deleteAllCourses()
        VDDataManager.sharedManager.addTenUsers()
        VDDataManager.sharedManager.addTenCourses()
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }
    
    func saveContext () {
        let context = VDDataManager.sharedManager.persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

