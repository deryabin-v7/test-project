//
//  VDCourseSpecial.swift
//  VDDZCoreData
//
//  Created by Victor Deryabin on 15.07.20.
//  Copyright © 2020 Victor Deryabin. All rights reserved.
//

import UIKit
import CoreData
class VDCourseSpecial: Special {
        var name: String?
        var predmet: String?
        var students = [VDUserSpecial]()
        var prepod: VDUserSpecial?
    static var courses = [VDCourseSpecial]()
        static func getCourseIndexByID( id:NSManagedObjectID) ->  Int? {
        
        for i in 0 ..< VDCourseSpecial.courses.count {
            if VDCourseSpecial.courses[i].ID == id {
                return i
            }
        }
        return nil
    }
    
    override subscript( string: String) -> String? {
        switch string {
        case "name":
            return name
        case "predmet":
            return predmet
        default:
            return nil
        }
    }
    static func deleteCourseByID( id:NSManagedObjectID) ->  Bool {
        for i in 0 ..< VDCourseSpecial.courses.count {
            if VDCourseSpecial.courses[i].ID == id {
                VDCourseSpecial.courses.remove(at: i)
                return true
            }
        }
        return false
    }
    
    func deleteStudentById(id: NSManagedObjectID) ->  Bool  {
        for i in 0 ..< self.students.count {
            if students[i].ID == id {
                self.students.remove(at: i)
                return true
            }
        }
        return false
    }
    
    static func fetchFromEntities( entity: VDCourse) {
        if getCourseIndexByID(id: entity.objectID) == nil {
            let uSpecial = VDCourseSpecial()
            uSpecial.name = entity.name
            uSpecial.ID = entity.objectID
            uSpecial.predmet = entity.predmet
            
            if entity.prepod != nil {
                if let usIndex = VDUserSpecial.getUserIndexByID(id: (entity.prepod?.objectID)!) {
            uSpecial.prepod = VDUserSpecial.users[usIndex]
                }
            }
            for case let obj as VDUser in entity.students! {
                if let usIndex = VDUserSpecial.getUserIndexByID(id: obj.objectID) {
                uSpecial.students.append(VDUserSpecial.users[usIndex])
                }
            }
            uSpecial.students.sortingBy(parameters: ["firstName","lastName"])
            courses.append(uSpecial)
        }
    }
}
