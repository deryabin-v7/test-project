//
//  VDPrepodOfCourseCheckViewController.swift
//  VDDZCoreData
//
//  Created by Victor Deryabin on 19.07.20.
//  Copyright © 2020 Victor Deryabin. All rights reserved.
//

import UIKit

protocol ChangePrepodOfCourse {
    func changePrepodOfCourse(checkedStudent: Int?)
}

class VDPrepodOfCourseCheckViewController: VDUserViewController {
    var delegate3: ChangePrepodOfCourse?
     var prepodIndex: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItems = []
        self.navigationItem.leftBarButtonItems = []
        let but = UIBarButtonItem.init(title: "Save", style: .plain, target: self, action: #selector(saveChoice(_:)))
        self.navigationItem.setRightBarButton(but, animated: true)
    }
    
    @objc func saveChoice(_ but: UIBarButtonItem) {
            delegate3!.changePrepodOfCourse(checkedStudent: prepodIndex)
            self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillLayoutSubviews() {
        if prepodIndex != nil {
        let cell = tableView?.cellForRow(at: IndexPath.init(row: prepodIndex!, section: 0))
        cell?.accessoryType = .checkmark
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.selectionStyle = .none
        if cell?.accessoryType == .checkmark {
            prepodIndex = nil
            cell?.accessoryType = .none
        }
        else {
            if prepodIndex != nil {
                let cell2 = tableView.cellForRow(at: IndexPath.init(row: prepodIndex!, section: 0))
                cell2?.accessoryType = .none
            }
            prepodIndex = indexPath.row
            cell?.accessoryType = .checkmark
        }
    }
}
