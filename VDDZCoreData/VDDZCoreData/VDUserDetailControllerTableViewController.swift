//
//  VDUserDetailControllerTableViewController.swift
//  VDDZCoreData
//
//  Created by Victor Deryabin on 12.07.20.
//  Copyright © 2020 Victor Deryabin. All rights reserved.
//

import UIKit
import CoreData

protocol DidUserDataChange {
    func changeUserDataAt(object: VDUserSpecial)
}

class VDUserDetailControllerTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,TextFieldChanged,ChangeCoursesOfStudent,ChangeCoursesForTeachingOfStudent {
    var delegate: DidUserDataChange?
    var user: VDUserSpecial?
    var nameData = ["firstName","lastName","adress"]
    var newData = [String:String]()
    let identifier2 = "UserDetailCell"
    @IBOutlet var tableView: UITableView?
    override func viewDidLoad() {
        super.viewDidLoad()
        //user = VDUserSpecial.getUserByID(myIndexObject)
        var nib = UINib.init(nibName: "detailcell", bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: "UserDetailCell")
        nib = UINib.init(nibName: "courseDescriptionCell", bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: "CourseCell")
        
        nib = UINib.init(nibName: "VDCourseCheckViewController", bundle: nil)
        self.tableView?.register(nib, forCellReuseIdentifier: "CoursesCheck")
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Save", style: .plain, target: self, action: #selector(saveData(_:)))
        newData[nameData[0]] = user?.firstName ?? ""
        newData[nameData[1]] = user?.lastName ?? ""
        newData[nameData[2]] = user?.adress ?? ""
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 50
        }
        return 90
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "User information"
        case 1:
            return "Courses for learning"
        case 2:
            return "Courses for teaching"
        default:
            NSLog("ERROR IN titleForHeaderInSection IN COURSE DETAIL VC!!!\n\n")
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath)
    {
        if (indexPath.row == 0 && (indexPath.section == 1 || indexPath.section == 2)) {
            let cell = tableView.cellForRow(at: indexPath)
            cell?.backgroundColor = UIColor.lightGray
        }
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        if (indexPath.row == 0 && (indexPath.section == 1 || indexPath.section == 2))  {
            cell?.backgroundColor = UIColor(red: 0.0 ,  green: 1.0 , blue: 140/255.0 , alpha: 0.2)
        }
        else {
            cell?.backgroundColor = UIColor.white
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == 0 && (indexPath.section == 1 || indexPath.section == 2)) {
            cell.backgroundColor = UIColor(red: 0.0 , green: 1.0 , blue: 140/255.0 , alpha: 0.2)
        }
        else {
            cell.backgroundColor = UIColor.white
        }
         cell.selectionStyle = .none
    }
    
    func changeCoursesOfStud(checkedCourses: [Bool]) {
        for index in 0 ..< checkedCourses.count {
            if checkedCourses[index] == false {
                VDDataManager.sharedManager.resignUserAsStudent(with: user!.ID!, fromCourseWith: VDCourseSpecial.courses[index].ID!)
            }
            else {
                VDDataManager.sharedManager.assignUserAsStudent(with: user!.ID!, onCourseWith: VDCourseSpecial.courses[index].ID!)
            }
        }
    }
    
    func changeCoursesForTeachingOfStud(checkedCourses: [Bool]) {
        for index in 0 ..< checkedCourses.count {
            if checkedCourses[index] == false {
                VDDataManager.sharedManager.resignUserAsTeacher(with: user!.ID!, fromCourseWith: VDCourseSpecial.courses[index].ID!)
            }
            else {
                VDDataManager.sharedManager.assignUserAsTeacher(with: user!.ID!, onCourseWith: VDCourseSpecial.courses[index].ID!) 
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (1...2).contains(indexPath.section) && indexPath.row == 0{
            let storyBoard = UIStoryboard.init(name: "MultipleCheckController", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "CheckController") as! VDCheckViewController
            vc.type = .students
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            var boolArray = [Bool](repeating: false, count: VDCourseSpecial.courses.count)
            if indexPath.section == 1 {
            for obj in (user?.courses)! {
                boolArray[VDCourseSpecial.getCourseIndexByID(id: obj.ID!)!] = true
                }
                vc.delegate3 = self as ChangeCoursesOfStudent
                vc.type = .learning
            }
            if indexPath.section == 2 {
                for obj in (user?.coursesForTeaching)! {
                    boolArray[VDCourseSpecial.getCourseIndexByID(id: obj.ID!)!] = true
                }
                vc.delegate4 = self as ChangeCoursesForTeachingOfStudent
                vc.type = .teaching
            }
            vc.checked = boolArray
            vc.delegate3 = self as ChangeCoursesOfStudent
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func saveData(_ but: UIBarButtonItem) {
        for i in 0..<nameData.count {
            let cell = tableView?.cellForRow(at: IndexPath.init(row: i, section: 0)) as! VDDetailCell
            cell.txtField?.endEditing(true)
        }
        if user == nil {
            user = VDUserSpecial()
        }
        user?.firstName = newData[nameData[0]]
        user?.lastName = newData[nameData[1]]
        user?.adress = newData[nameData[2]]
        
        delegate?.changeUserDataAt(object: user!)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func changeDictionaryData(name: String, value: String) {
        newData[name] = value
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return nameData.count
        case 1: return (user?.courses.count)! + 1
        case 2: return (user?.coursesForTeaching.count)! + 1
        default: return 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        VDDataManager.sharedManager.updateCourseBD()
        VDDataManager.sharedManager.updateUserBD()
        if user?.ID != nil {
            user = VDUserSpecial.users[VDUserSpecial.getUserIndexByID(id: user!.ID!)!]
            self.tableView?.reloadData()
        }
        self.tableView?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0: let cell = self.tableView?.dequeueReusableCell(withIdentifier: identifier2) as! VDDetailCell
        cell.delegate1 = self as TextFieldChanged
        cell.cellMeaning = nameData[indexPath.row]
        
        cell.label?.text = nameData[indexPath.row]
        cell.txtField?.text = newData[nameData[indexPath.row]]
        return cell
        case 1,2:
            let row = indexPath.row - 1
            var cell = VDMyCourseCell()
            let identifier = "CourseCell"
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as! VDMyCourseCell
            if indexPath.row == 0 {
                cell.name?.text = " "
                cell.predmet?.text = " "
                cell.prepod?.text = "Add Course"
                return cell
            }
            
            if indexPath.section == 1 {
                cell.configureCell(withObject:  (user?.courses[row])!)
            }
            else if indexPath.section == 2 {
                cell.configureCell(withObject: (user?.coursesForTeaching[row])!)
            }
                return cell
        default: break
        }
        return  UITableViewCell()
    }
}
