//
//  VDCourseViewController.swift
//  VDDZCoreData
//
//  Created by Victor Deryabin on 14.07.20.
//  Copyright © 2020 Victor Deryabin. All rights reserved.
//

import UIKit
import CoreData
class VDCourseViewController: ViewController, DidCourseDataChange {
    var temporaryCourseID: NSManagedObjectID?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource.setCellType(cellIdentifier: "CourseCell")
        let but = UIBarButtonItem.init(barButtonSystemItem: .edit, target: self, action: #selector(myEditing(but:)))
        self.navigationItem.leftBarButtonItem = but
        
        let but2 = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
        
        self.tableView?.reloadData()
        self.navigationItem.rightBarButtonItem = but2
        // Do any additional setup after loading the view.
    }
    
    func changeCourseDataAt( object: VDCourseSpecial) {
        temporaryCourseID = nil
        
        VDDataManager.sharedManager.updateCourse(course: object)
        VDDataManager.sharedManager.updateUserBD()
        tableView?.reloadData()
    }
    
    func callDetailViewController( myIndexPath: NSIndexPath?) {
       
        let vc =  self.storyboard?.instantiateViewController(withIdentifier: "detailCourseID") as! VDCourseDetailTableViewController
        vc.delegate = self as DidCourseDataChange
        if myIndexPath == nil {
            let id = VDDataManager.sharedManager.addEmptyCourse()
            vc.course = VDCourseSpecial()
            vc.course?.ID = id
            temporaryCourseID = id
           
        }
        else {
        vc.course = VDCourseSpecial.courses[myIndexPath!.row]
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        callDetailViewController(myIndexPath: indexPath as NSIndexPath)
        
    }
    
    @objc func insertNewObject(_ sender: Any) {
        callDetailViewController(myIndexPath: nil)
        self.tableView?.reloadData()
    }
    
    override func createDict() -> [String: [Special]]{
        return ["Courses":VDCourseSpecial.courses]
    }
   
    override func viewWillAppear(_ animated: Bool) {
        if temporaryCourseID != nil {
            VDDataManager.sharedManager.deleteByID(id: temporaryCourseID!)
            temporaryCourseID = nil
        }
        VDDataManager.sharedManager.updateCourseBD()
        VDDataManager.sharedManager.updateUserBD()
        dataSource.setDataDictionary(data: createDict())
        self.tableView?.reloadData()
    }
}
